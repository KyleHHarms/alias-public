RED='\033[0;31m'
NC='\[\033[0m\]'
LR='\[\033[1;31m\]'
LP='\[\033[1;35m\]'
LCYAN='\[\033[1;36m\]'
GREEN='\[\033[0;32m\]'
trap 'printf "\n"' DEBUG;
export PS1="\n[${LR}\u@\H${NC}][${LP}\w${NC}][${LCYAN}\A${NC}] \[$(tput sgr0)\]\n${GREEN}(Δ)${NC} ";

echo -e "The disease...I feel it spreading"

function aplog()        #Opens the apache log.
{
    less /usr/local/apache/logs/error_log
}

function dog()          #Grabs relevant DNS records for ().
{
	dig any +nocomments +nostats +nottl $1 | grep "IN" | grep -v "ANY" | grep -E "[AMXCNESRVOT]{1}" | sort
}

function restime ()
{
curl -s -w 'Testing Website Response Time for : %{url_effective}\n\nLookup Time:\t\t%{time_namelookup}\nConnect Time:\t\t%{time_connect}\nAppCon Time:\t\t%{time_appconnect}\nRedirect Time:\t\t%{time_redirect}\nPre-transfer Time:\t%{time_pretransfer}\nStart-transfer Time:\t%{time_starttransfer}\n\nTotal Time:\t\t%{time_total}\n' -o /dev/null $1
}

function mylog()
{
    less /var/lib/mysql/$HOSTNAME.err 
}

function inodes()
{
    find $pwd -xdev -printf '%h\n' | sort | uniq -c | sort -k 1 -n
}
